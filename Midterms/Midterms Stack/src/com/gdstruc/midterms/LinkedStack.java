package com.gdstruc.midterms;

import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Random;

public class LinkedStack {
    private LinkedList<Card> stack;
    private LinkedList<Card> hand;
    private LinkedList<Card> pile;

    public LinkedStack()
    {
        stack = new LinkedList<Card>();
        hand = new LinkedList<Card>();
        pile = new LinkedList<Card>();
    }

    public void push(Card cardType)
    {
        stack.push(cardType);
    }

    public boolean isEmpty()
    {
        return stack.isEmpty();
    }

    public Card pop()
    {
        return stack.pop();
    }

    public Card peek()
    {
        return stack.peek();
    }

    public void printStack()
    {
        ListIterator<Card> iterator = stack.listIterator();

        System.out.println("Printing stack:");

        while(iterator.hasNext())
        {
            System.out.println(iterator.next());
        }
    }

    // Commands Functions
    public void Draw()
    {
        Random gen = new Random();
        int drawCards = (gen.nextInt(5) + 1);
        for (int i = 1; i < drawCards; i++)
        {
            hand.push(stack.pop());
        }
    }

    public void Discard()
    {
        Random gen = new Random();
        int drawCards = (gen.nextInt(5) + 1);
        for (int i = 1; i < drawCards; i++)
        {
            pile.push(hand.pop());
        }
    }

    public void Get()
    {
        Random gen = new Random();
        int drawCards = (gen.nextInt(5) + 1);
        for (int i = 1; i < drawCards; i++)
        {
            hand.push(pile.pop());
        }
    }

    // Print Functions
    public void printHand()
    {
        System.out.println("Player Hand: " + hand.size());
    }

    public void printNumDeckCards()
    {
        System.out.println("Deck: " + stack.size());
    }

    public void printPile()
    {
        System.out.println("Deck: " + pile.size());
    }
}
