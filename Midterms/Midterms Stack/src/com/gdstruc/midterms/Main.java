package com.gdstruc.midterms;

import java.util.Random;

public class Main {

    public static void main(String[] args) {
	LinkedStack stack = new LinkedStack();
	LinkedStack hand = new LinkedStack();
	LinkedStack pile = new LinkedStack();

	// Card Sets
	stack.push(new Card("Ace of Hearts")); // 10 Hearts Card Set
	stack.push(new Card("2 of Hearts"));
	stack.push(new Card("3 of Hearts"));
	stack.push(new Card("4 of Hearts"));
	stack.push(new Card("5 of Hearts"));
	stack.push(new Card("6 of Hearts"));
	stack.push(new Card("7 of Hearts"));
	stack.push(new Card("8 of Hearts"));
	stack.push(new Card("9 of Hearts"));
	stack.push(new Card("10 of Hearts"));

	stack.push(new Card("Ace of Spades")); // 10 Spades Card Set
	stack.push(new Card("2 of Spades"));
	stack.push(new Card("3 of Spades"));
	stack.push(new Card("4 of Spades"));
	stack.push(new Card("5 of Spades"));
	stack.push(new Card("6 of Spades"));
	stack.push(new Card("7 of Spades"));
	stack.push(new Card("8 of Spades"));
	stack.push(new Card("9 of Spades"));
	stack.push(new Card("10 of Spades"));

	stack.push(new Card("Ace of Clubs")); // 10 Clubs Card Set
	stack.push(new Card("2 of Clubs"));
	stack.push(new Card("3 of Clubs"));
	stack.push(new Card("4 of Clubs"));
	stack.push(new Card("5 of Clubs"));
	stack.push(new Card("6 of Clubs"));
	stack.push(new Card("7 of Clubs"));
	stack.push(new Card("8 of Clubs"));
	stack.push(new Card("9 of Clubs"));
	stack.push(new Card("10 of Clubs"));

	// Should be in while loop
	// Game finishes when Player Deck is Empty
		while(hand.isEmpty())
		{
			Random gen = new Random();
			int randCommand = (gen.nextInt(3) + 1);
			if(randCommand == 1) {
				hand.Draw();
			}
			if(randCommand == 2) {
				hand.Discard();
			}
			if(randCommand == 3) {
				hand.Get();
			}
			stack.printHand(); // Prints Player Hand
			stack.printNumDeckCards(); // Prints Player Deck
			stack.printPile(); // Prints Discarded Pile
		}
	//stack.printStack();


	//System.out.println("Popping: " + stack.pop());

	//stack.printStack();
    }
}
