package com.gdstruc.quiz3;

import java.util.NoSuchElementException;
import java.util.Random;

public class ArrayQueue {

    private Player[] queue;
    private int front;
    private int back;

    public ArrayQueue(int capacity)
    {
        queue = new Player[capacity];
    }

    public void add(Player player)
    {
        if(back == queue.length)
        {
            Player[] newArray = new Player[queue.length * 2];
            System.arraycopy(queue, 0, newArray, 0, queue.length);
            queue = newArray;
        }

        queue[back] = player;
        back++;
    }

    public Player remove()
    {
        if (size() == 0)
        {
            throw new NoSuchElementException();
        }

        Player removedPlayer = queue[front];
        queue[front] = null;
        front++;

        if (size() == 0) // reset trackers when queue is empty
        {
            front = 0;
            back = 0;
        }

        return removedPlayer;
    }

    public Player peek()
    {
        if(size() == 0)
        {
            throw new NoSuchElementException();
        }
        return queue[front];
    }

    public int size()
    {
        return back - front;
    }

    public void printQueue()
    {
        for (int i = front; i < back; i++)
        {
            System.out.println(queue[i]);
        }
    }

    public static int randomQueue()
    {
        Random gen = new Random();
        return gen.nextInt(7) + 1;
    }

    public void matchMaking()
    {
        int games = 1;
        while(games == 10)
        {
            //while (size() >= 5)
            //{
                int counter = randomQueue();
                for (int i = 0; i >= counter; i++)
                {
                    add(new Player(1, "zen", 100));
/*                    add(new Player(2, "Uni", 98));
                    add(new Player(3, "Lynx", 86));
                    add(new Player(4, "Papatatis", 74));
                    add(new Player(5, "Mortem", 63));
                    add(new Player(6, "Trip", 51));
                    add(new Player(7, "Lucid", 40));*/
                }
            //}
            if (size() == 5)
            {
                System.out.println("Entering matchmaking...");
                System.out.println("Players are joining: ");
                printQueue();
            }
            if (size() >= 5)
            {
                for (int i = 1; i <= 5; i++)
                {
                    remove();
                }
            }
            games++;
        }
        System.out.println("Player Queue is done.");
    }
}
