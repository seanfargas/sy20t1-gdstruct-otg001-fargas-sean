package com.gdstruc.quiz;

// Bonus Quiz 5 - Own Search Algorithm
// Simplify Algorithm
// Sorted Algorithm
// Requires steps by check through beginning, middle and end of numbers only then the user input can be found.

public class Main {

    public static void main(String[] args) {

        int[] array = { 10, 25, 30, 45, 50, 65, 70};

        if(simplifyAlgorithm(array, 45)) // Change user input here to detect if the number that you are searching can be or cannot be found in the array.
            System.out.println("FOUND Your Number! =)");
        else
            System.out.println("Number CANNOT Be Found =(");
    }

    public static boolean simplifyAlgorithm(int[] array, int userInput)
    {
        int begin = 0;
        int end = array.length - 1;
        int middle = (begin + end) / 2;

        while(begin <= end)
        {
            if(array[middle] < userInput)
                begin = middle + 1;
            else if (array[middle] > userInput)
                end = middle - 1;
            else
                return true;
            middle = (begin + end) / 2;
        }
        return false;
    }
}
