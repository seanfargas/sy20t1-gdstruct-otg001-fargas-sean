package com.gdstruc.module2;

import java.util.NoSuchElementException;
import java.util.Objects;

public class PlayerLinkedList {
    private PlayerNode head;

    public void addToFront(Player player)
    {
        PlayerNode playerNode = new PlayerNode(player);
        playerNode.setNextPlayer(head);
        head = playerNode;
    }

    public void printList()
    {
        PlayerNode current = head;
        System.out.print("HEAD -> ");
        while(current!= null)
        {
            System.out.print(current);
            System.out.print(" -> ");
            current = current.getNextPlayer();
        }
        System.out.println("null");
    }

    public PlayerNode deleteFirst()
    {
        if(head != null)
        {
            head = head.getNextPlayer();
        }
        return head;
    }

    public int length()
    {
        int count = 0;
        PlayerNode current = head;
        while (current != null)
        {
            count++;
            current = current.getNextPlayer();
        }
        return count;
    }

/*    public boolean containsFunction(Object o)
    {
        PlayerNode current = head;
        for(PlayerNode e : this)
        {
            if(Objects.equals(o, e))
            {
                return true;
            }
        }
        return false;
    }*/
}
