package com.gdstruc.module2;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        Player asuna = new Player(1, "Asuna", 100);
        Player lethalBacon = new Player(2, "LethalBacon", 205);
        Player hpDeskjet = new Player(3, "HPDeskjet", 34);

        PlayerLinkedList playerLinkedList = new PlayerLinkedList();

        playerLinkedList.addToFront(asuna);
        playerLinkedList.addToFront(lethalBacon);
        playerLinkedList.addToFront(hpDeskjet);

        //Printing Elements
        System.out.println("Linked List before removing first element: ");
        playerLinkedList.printList();
        System.out.println("Length of Linked List: " + playerLinkedList.length());
        //System.out.println(playerLinkedList.containsFunction());

        System.out.println("Linked List after removing first element: ");

        playerLinkedList.deleteFirst();
        playerLinkedList.printList();
        System.out.println("Length of Linked List: " + playerLinkedList.length());
    }
}
