package com.company;

public class Main {

    public static void main(String[] args) {

        int[] numbers = new int[7];

        numbers[0] = 68;
        numbers[1] = 419;
        numbers[2] = -32;
        numbers[3] = 38;
        numbers[4] = 54;
        numbers[5] = 82;
        numbers[6] = -4;

        // Selection Sort
        System.out.println("Quiz 1 Selection Sort Smallest Value First");

        System.out.println("\nBefore Selection Sort:");
        printArrayElements(numbers);

        selectionSort(numbers);

        System.out.println("\n\nAfter Selection Sort:");
        printArrayElements(numbers);
    }

    private static void selectionSort(int[] arr)
    {
        for (int lastSortedIndex = arr.length - 1; lastSortedIndex > 0; lastSortedIndex--)
        {
            int smallestIndex = 0;

            for (int i = 1; i <= lastSortedIndex; i++)
            {
                if (arr[i] > arr[smallestIndex])
                {
                    smallestIndex = i;
                }
            }

            int temp = arr[smallestIndex];
            arr[smallestIndex] = arr[lastSortedIndex];
            arr[lastSortedIndex] = temp;

        }
    }

    private static void printArrayElements(int[] arr)
    {
        for (int j : arr) {
            System.out.print(j + " ");
        }
    }
}
