package com.gdstruc.quiz4;

public class Main {

	// Sorry about the null problems sir
	// It was working before, but once I opened to check my code again
	// before sending it, it suddenly had null pointers after some modifications
	// I checked the disc group although I didn't have enough time to pass it within the deadline
	// while fixing it.

    public static void main(String[] args) {
	Player ploo = new Player(134, "Plooful", 135);
	Player wardell = new Player(536, "TSM Wardell", 640);
	Player deadlyJimmy = new Player(32, "DeadlyJimmy", 34);
	Player subroza = new Player(4931, "Subroza", 604);
	Player annieDro = new Player(6919, "C9 Annie", 593);

	SimpleHashtable hashtable = new SimpleHashtable();
	hashtable.put(ploo.getUserName(), ploo);
	hashtable.put(wardell.getUserName(), wardell);
	hashtable.put(deadlyJimmy.getUserName(), deadlyJimmy);
	hashtable.put(subroza.getUserName(), subroza);
	hashtable.put(annieDro.getUserName(), annieDro);


	//hashtable.printHashtable();
		//System.out.println(hashtable.get("Subroza"));

		System.out.println("[Player Line-up]");

		hashtable.printHashtable();

		System.out.print(System.lineSeparator());

		System.out.println("[Input Player to Be Removed From Line-up]");

		hashtable.removePlayer(deadlyJimmy.getUserName(), deadlyJimmy);

		System.out.print(System.lineSeparator());

		System.out.println("[Finalized Player Line-up]");

		hashtable.printHashtable();
    }
}
